const form = document.querySelector('.password-form');
const errorMessage = document.querySelector('.error');
const inputParent = form.querySelectorAll('.input-wrapper');
const inputItem = document.querySelectorAll('.input-password');
let btnSubmit = form.querySelector('.btn-submit');

inputParent.forEach(() => addEventListener('click', changePass));
btnSubmit = addEventListener('click', comparePass);

function changePass(event) {
    const iconEye = event.target;
    if (iconEye.nodeName === 'I' && iconEye.classList.contains('icon-password')) {
        const inputPassword = iconEye.parentElement.querySelector('.input-password');
        iconEye.classList.toggle('fa-eye-slash');
        iconEye.classList.toggle('fa-eye');
        inputPassword.type = (inputPassword.type === 'password') ? 'text' : 'password';
    }
}

function comparePass(event) {
    const senderSubmit = event.target;
    if (senderSubmit.nodeName === 'BUTTON' && senderSubmit.classList.contains('btn-submit')) {
        event.preventDefault();
        if (inputItem[0].value === '') {
            errorMessage.innerText = 'Треба ввести значення';
            errorMessage.style.color = 'red';
        }
        else if (inputItem[0].value === inputItem[1].value) {
            errorMessage.innerText = '';
            alert(`You are welcome`);
        } else {
            errorMessage.innerText = 'Потрібно ввести однакові значення';
            errorMessage.style.color = 'red';
        }
    }

}
